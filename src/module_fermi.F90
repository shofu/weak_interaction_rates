module module_fermi
  implicit none
  
  private
  public :: init_fermi_table, interp_fermi_table, fermik, fermi43_table, fermi2_to_eta, ffn_fermi

  ! fermi integral of order 2,3 table
  real(8),parameter :: pi = 4d0*atan(1d0)
  real(8) :: fermi_eta_min, fermi_eta_max, fermi_deta
  integer :: fermi_n_eta
  real(8),allocatable :: fermi_eta_table(:), fermi1_table(:), fermi2_table(:), fermi3_table(:), fermi4_table(:), fermi5_table(:)

contains

  subroutine fermik(eta,k,fint)
    real(8),intent(in) :: eta
    integer,intent(in) :: k
    real(8),intent(out) :: fint
    
    real(8) :: xfin,dx
    real(8) :: x, x_1,x_2,x_3,x_4, ex_4, f_1,f_2,f_3,f_4, residual
    
    integer :: i
    
    xfin = max(1000.d0,10.d0*eta)
    dx = 0.01d0
    fint = 0.d0
    do i=0,nint(xfin/dx)
       x = dx*dble(i)
       
       ! x_1 = x;          ex_1 = max(-3d2,min(3d2,x_1+xl-eta))
       ! f_1 = (x_1 + xl + zeta)**(2+ih) * ( (x_1 + xl)**2 - et**2 ) * sqrt(1.d0 - et**2/(x_1 + xl)**2) *1.d0/(exp(ex_1)+1d0)
       ! x_2 = x+0.5d0*dx; ex_2 = max(-3d2,min(3d2,x_2+xl-eta))
       ! f_2 = (x_2 + xl + zeta)**(2+ih) * ( (x_2 + xl)**2 - et**2 ) * sqrt(1.d0 - et**2/(x_2 + xl)**2) *1.d0/(exp(ex_2)+1d0)
       ! x_3 = x+0.5d0*dx; ex_3 = max(-3d2,min(3d2,x_3+xl-eta))
       ! f_3 = (x_3 + xl + zeta)**(2+ih) * ( (x_3 + xl)**2 - et**2 ) * sqrt(1.d0 - et**2/(x_3 + xl)**2) *1.d0/(exp(ex_3)+1d0)
       ! x_4 = x+dx;       ex_4 = max(-3d2,min(3d2,x_4+xl-eta))
       ! f_4 = (x_4 + xl + zeta)**(2+ih) * ( (x_4 + xl)**2 - et**2 ) * sqrt(1.d0 - et**2/(x_4 + xl)**2) *1.d0/(exp(ex_4)+1d0)
       
       x_1 = x
       x_2 = x+0.5d0*dx
       x_3 = x+0.5d0*dx
       x_4 = x+dx
       f_1 = x_1**k/(exp(max(-3d2,min(3d2,x_1-eta)))+1d0)
       f_2 = x_2**k/(exp(max(-3d2,min(3d2,x_2-eta)))+1d0)
       f_3 = f_2
       f_4 = x_4**k/(exp(max(-3d2,min(3d2,x_4-eta)))+1d0)
       
       fint = fint + (f_1 + 2d0*f_2 + 2d0*f_3 + f_4)/6d0*dx
       
       ex_4 = max(-3d2,min(3d2,x_4))
       
       !residual = exp(-ex_4)* ( (x_4**4 + 4.d0*x_4**3 + 12.d0*x_4**2 + 24.d0*x_4 + 24.d0)  * (5d0*x_4)**ih + 120d0*dble(ih) )
       residual = exp(-ex_4)*x_4**k
       
       !if(x>10.d0*abs(eta-xl).and.x>10.d0*abs(zeta).and.x>10.d0*et.and.x>10.d0*xl.and.fint > 1.d15*residual) goto 10
       if(x > 10.d0*abs(eta) .and. fint > 1.d15*residual) goto 10
    enddo

10  continue

    return
  end subroutine fermik

  subroutine init_fermi_table(fn)
    character(*),intent(in) :: fn
    
    integer :: access
    integer :: n_eta,i_eta
    real(8) :: eta_min, eta_max

    if(access(fn," ") == 0)then
       call read_fermi_table(fn)
    else
       n_eta=2001
       eta_min = -1d2
       eta_max = 1d2

       write(6,*) "# Fermi table will be created."
       write(6,*) "# range:"
       write(6,*) "# eta",eta_min,eta_max,n_eta

       call make_fermi_table(eta_min,eta_max,n_eta)
       
       open(11,file=fn,status="replace")
       write(11,'("#",i10)') fermi_n_eta
       do i_eta = 1,fermi_n_eta
          write(11,'(99es16.7e3)') fermi_eta_table(i_eta), fermi1_table(i_eta), fermi2_table(i_eta), fermi3_table(i_eta), fermi4_table(i_eta), fermi5_table(i_eta)
       enddo
       close(11)
       
    endif
    
  end subroutine init_fermi_table

  subroutine read_fermi_table(fn)
    character(*),intent(in) :: fn
    integer :: i_eta

    write(6,*) "# read fermi table"
    
    open(11,file=fn,status="old")
    read(11,'(1x,i10)') fermi_n_eta
    allocate(fermi_eta_table(fermi_n_eta), fermi1_table(fermi_n_eta), fermi2_table(fermi_n_eta), fermi3_table(fermi_n_eta), fermi4_table(fermi_n_eta), fermi5_table(fermi_n_eta))
    do i_eta = 1,fermi_n_eta
       read(11,*) fermi_eta_table(i_eta), fermi1_table(i_eta), fermi2_table(i_eta), fermi3_table(i_eta), fermi4_table(i_eta), fermi5_table(i_eta)
    enddo
    close(11)
    fermi_eta_min = fermi_eta_table(1)
    fermi_eta_max = fermi_eta_table(fermi_n_eta)
    fermi_deta    = (fermi_eta_max-fermi_eta_min)/dble(fermi_n_eta-1)

    write(6,*) "# read fermi table done"
    write(6,*) "#", fermi_n_eta
    
  end subroutine read_fermi_table
  
  subroutine make_fermi_table(eta_min,eta_max,n_eta)
    integer,intent(in) :: n_eta
    real(8),intent(in) :: eta_min, eta_max

    integer :: i_eta
    real(8) :: eta,f1,f2,f3,f4,f5
    
    fermi_eta_min = eta_min
    fermi_eta_max = eta_max
    fermi_n_eta = n_eta
    fermi_deta = (fermi_eta_max-fermi_eta_min)/dble(fermi_n_eta-1)
    
    write(6,*) "# eta_min,eta_max : ", fermi_eta_min, fermi_eta_max
    write(6,*) "# Neta, deta : ", fermi_n_eta, fermi_deta

    allocate(fermi_eta_table(fermi_n_eta), fermi1_table(fermi_n_eta), fermi2_table(fermi_n_eta), fermi3_table(fermi_n_eta), fermi4_table(fermi_n_eta), fermi5_table(fermi_n_eta))
    
    do i_eta = 1,fermi_n_eta
       eta = fermi_eta_min + fermi_deta*dble(i_eta-1)
       call fermik(eta,1,f1)
       call fermik(eta,2,f2)
       call fermik(eta,3,f3)
       call fermik(eta,4,f4)
       call fermik(eta,5,f5)
       fermi_eta_table(i_eta) = eta
       fermi1_table(i_eta) = f1
       fermi2_table(i_eta) = f2
       fermi3_table(i_eta) = f3
       fermi4_table(i_eta) = f4
       fermi5_table(i_eta) = f5
       write(6,*) "#", i_eta
    enddo
    
  end subroutine make_fermi_table
  
  subroutine interp_fermi_table(eta,fermi1,fermi2,fermi3,fermi4,fermi5)
    
    real(8),intent(in) :: eta
    real(8),intent(out) :: fermi1,fermi2,fermi3,fermi4,fermi5
    
    integer :: i_eta
    real(8) :: ee0,ee1

    if(eta>fermi_eta_max)then
       fermi1 = eta**2/2d0 + 2d0
       fermi2 = eta**3/3d0 + 4d0*eta
       fermi3 = eta**4/4d0 + pi**2/2d0*eta**2
       fermi4 = eta**5/5d0 + 2d0/3d0*pi**2*eta**3
       fermi5 = eta**6/6d0 + 5d0/6d0*pi**2*eta**4
    elseif(eta<fermi_eta_min)then
       fermi1 = 2d0*exp(eta)
       fermi2 = 2d0*exp(eta)
       fermi3 = 6d0*exp(eta)
       fermi4 = 24d0*exp(eta)
       fermi5 = 120d0*exp(eta)
    else
       i_eta = min(fermi_n_eta-1,int((eta-fermi_eta_min)/fermi_deta) + 1)
       
       if(eta>0d0.and.fermi_eta_table(i_eta)>0d0)then
          ! For eta>0, F_k(eta) ~ polynomial function -> log-log interpolation
          ! write(6,*) i_eta, eta, fermi_eta_table(i_eta), fermi_eta_table(i_eta+1)
          ee1 = (log10(eta)-log10(fermi_eta_table(i_eta)))/(log10(fermi_eta_table(i_eta+1)) - log10(fermi_eta_table(i_eta)))
          ee0 = 1d0-ee1
          fermi1 = 10d0**(ee1*log10(fermi1_table(i_eta+1)) + ee0*log10(fermi1_table(i_eta)))
          fermi2 = 10d0**(ee1*log10(fermi2_table(i_eta+1)) + ee0*log10(fermi2_table(i_eta)))
          fermi3 = 10d0**(ee1*log10(fermi3_table(i_eta+1)) + ee0*log10(fermi3_table(i_eta)))
          fermi4 = 10d0**(ee1*log10(fermi4_table(i_eta+1)) + ee0*log10(fermi4_table(i_eta)))
          fermi5 = 10d0**(ee1*log10(fermi5_table(i_eta+1)) + ee0*log10(fermi5_table(i_eta)))

       else
          ! For eta<0, F_k(eta) ~ exponential function -> linear-log interpolation
          ee1 = (eta-fermi_eta_table(i_eta))/fermi_deta
          ee0 = 1d0-ee1
          fermi1 = 10d0**(ee1*log10(fermi1_table(i_eta+1)) + ee0*log10(fermi1_table(i_eta)))
          fermi2 = 10d0**(ee1*log10(fermi2_table(i_eta+1)) + ee0*log10(fermi2_table(i_eta)))
          fermi3 = 10d0**(ee1*log10(fermi3_table(i_eta+1)) + ee0*log10(fermi3_table(i_eta)))
          fermi4 = 10d0**(ee1*log10(fermi4_table(i_eta+1)) + ee0*log10(fermi4_table(i_eta)))
          fermi5 = 10d0**(ee1*log10(fermi5_table(i_eta+1)) + ee0*log10(fermi5_table(i_eta)))
       endif
    endif
    
  end subroutine interp_fermi_table

  subroutine fermi43_table(eta,fermi43)
    
    real(8),intent(in) :: eta
    real(8),intent(out) :: fermi43
    
    real(8),parameter :: tol = 1d-8
    integer,parameter :: itrlim=30
    integer :: itr_out,return_code
    real(8) :: err

    real(8) :: fermi1,fermi2,fermi3,fermi4,fermi5
    
    if(eta<fermi_eta_min)then
       fermi43 = 4d0
    else
       call interp_fermi_table(eta,fermi1,fermi2,fermi3,fermi4,fermi5)
       fermi43 = fermi4/fermi3
    endif

  end subroutine fermi43_table

  subroutine fermi2_to_eta(fermi2,eta,eta_guess)
    real(8),intent(in) :: fermi2
    real(8),intent(out) :: eta
    real(8),intent(in),optional :: eta_guess
    
    real(8) :: eta_min, eta_max

    integer :: itr_out
    integer,parameter :: itrlim=40
    real(8),parameter :: tol = 1d-8
    integer :: return_code
    real(8) :: err

    write(6,*) "#", fermi2_table(1), fermi2_table(fermi_n_eta)
    if    (fermi2 > fermi2_table(fermi_n_eta))then
       eta = fermi_eta_table(fermi_n_eta)
       return
    elseif(fermi2 < fermi2_table(1))then
       eta = fermi_eta_table(1)
       return
    endif

    eta_min = fermi_eta_min
    eta_max = fermi_eta_max

    if(present(eta_guess))then

       if(func(eta_guess)*func(eta_min) > 0d0)then
          eta_min = eta_guess
       else
          eta_max = eta_guess
       endif
    endif
    
    call illinois_rootfinding_check(eta,err,itr_out,eta_min,eta_max,tol,itrlim,return_code, func)

    ! block
    !   real(8) :: fermi2_bar,fermi3_bar,fermi4_bar,fermi5_bar
    !   call interp_fermi_table(eta,fermi2_bar,fermi3_bar,fermi4_bar,fermi5_bar)
    !   write(6,'(i5,99es12.4)') itr_out,eta, fermi2_bar
    ! end block
    ! stop

    contains

      real(8) function func(eta_bar)
        real(8),intent(in) :: eta_bar
        real(8) :: fermi1_bar,fermi2_bar,fermi3_bar,fermi4_bar,fermi5_bar
        
        call interp_fermi_table(eta_bar,fermi1_bar,fermi2_bar,fermi3_bar,fermi4_bar,fermi5_bar)
        
        func = log10(fermi2_bar) - log10(fermi2)

      end function func

    
  end subroutine fermi2_to_eta

  
  subroutine FFN_fermi(eta,fermi1,fermi2,fermi3,fermi4,fermi5)
    
    real(8),intent(in) :: eta
    real(8),intent(out) :: fermi1,fermi2,fermi3,fermi4,fermi5
    
    real(8) :: ex,eta2,eta3,eta4,eta5,eta6
    if(eta<0d0)then
       ex = exp(eta)
       fermi1 =       ex
       fermi2 = 2d0  *ex
       fermi3 = 6d0  *ex
       fermi4 = 24d0 *ex
       fermi5 = 120d0*ex
    else
       ex = exp(-eta)
       eta2 = eta*eta
       eta3 = eta*eta2
       eta4 = eta*eta3
       eta5 = eta*eta4
       eta6 = eta*eta5
       
       fermi1 = eta2/2d0 + 2d0 - ex
       fermi2 = eta3/3d0 + 4d0*eta +2d0*ex
       fermi3 = eta4/4d0 + pi*pi/2d0*eta2 + 12d0 - 6d0*ex
       fermi4 = eta5/5d0 + 2d0*pi*pi/3d0*eta3 + 48d0*eta + 24d0*ex
       fermi5 = eta6/6d0 + 5d0*pi*pi/6d0*eta4 + 7d0*pi*pi*pi*pi/6d0*eta2 + 240d0 - 120*ex
    endif
    
  end subroutine FFN_fermi
  
end module module_fermi
