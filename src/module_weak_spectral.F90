module module_weak_spectral
#include "defs.h"
  use phys_const

  implicit none

  public :: electron_capture_emissivity, positron_capture_emissivity, crosssection_nue_n_abs_incl_stimulated, crosssection_nueb_p_abs_incl_stimulated

  real(8),parameter :: erg_to_mev = 1.d0/mev_to_erg

  real(8),parameter :: hbc = hbar*clight
  real(8),parameter :: gfmev=gfermi/1d6

  real(8),parameter :: dmnp = (mn-mp)*clight**2*erg_to_mev
  real(8),parameter :: memev = me*clight**2*erg_to_mev
  real(8),parameter :: mpmev = mp*clight**2*erg_to_mev
  real(8),parameter :: mumev = amu*clight**2*erg_to_mev
  real(8),parameter :: mnmev = amu*clight**2*erg_to_mev

  real(8),parameter :: sigma0 = gfmev**2 * memev**2 * 4.d0/pi *(hbc*erg_to_mev)**2
  real(8),parameter :: ga = -1.26d0
  real(8),parameter :: ga2= ga*ga

contains

    real(8) function electron_capture_emissivity(ener,temp_mev,eta_e) result(emissivity)
      ! energy of neutrino in MeV
      real(8),intent(in) :: ener
      ! Temperature in MeV
      real(8),intent(in) :: temp_mev
      ! degeneracy parrameter of electrons
      real(8),intent(in) :: eta_e
      
      real(8) :: ener_ele, fermi_ele
      real(8) :: weak_mag !dimensionless
      
      
      ener_ele = ener+dmnp
      if(ener_ele - memev >= 0d0)then
         weak_mag = weak_mag_corr(ener,0)
         fermi_ele = 1d0 / (1d0 + exp( min(3d2, max(-3d2, (ener_ele)/temp_mev - eta_e ))) )
         ! ! 1/s/MeV/srad
         !emissivity = cap_nrate_base &
         !     *ener**2*ener_ele**2*sqrt(1d0 - memev**2/ener_ele**2)*fermi_ele/(4d0*pi) * weak_mag
         ! cm^2
         emissivity = sigma0*(1d0+3d0*ga2)/4d0 &
              *(ener_ele/memev)**2*sqrt(1d0 - (memev/ener_ele)**2)*fermi_ele * weak_mag
      else
         emissivity = 0d0
      endif
    
    
  end function electron_capture_emissivity

  real(8) function positron_capture_emissivity(ener,temp_mev,eta_e) result(emissivity)
    ! energy of neutrino in MeV
    real(8),intent(in) :: ener
    ! Temperature in MeV
    real(8),intent(in) :: temp_mev
    ! degeneracy parrameter of electrons
    real(8),intent(in) :: eta_e

    real(8) :: ener_pos, fermi_pos
    real(8) :: weak_mag !dimensionless

    
    ener_pos = ener-dmnp
    if(ener_pos - memev >= 0d0)then
       weak_mag = weak_mag_corr(ener,1)
       fermi_pos = 1d0 / (1d0 + exp( min(3d2, max(-3d2, (ener_pos)/temp_mev + eta_e ))) )
       ! ! 1/s/MeV/srad
       ! emissivity = cap_nrate_base &
       !      *ener**2*ener_pos**2*sqrt(1d0 - memev**2/ener_pos**2)*fermi_pos/(4d0*pi) * weak_mag
       ! cm^2
       emissivity = sigma0*(1d0+3d0*ga2)/4d0 &
            *(ener_pos/memev)**2*sqrt(1d0 - (memev/ener_pos)**2)*fermi_pos * weak_mag
    else
       emissivity = 0d0
    endif
    
  end function positron_capture_emissivity

  real(8) function crosssection_nue_n_abs_incl_stimulated(ener, temp_mev, eta_e, muhat) result(crosssection)
    use const
    ! energy of neutrino in MeV
    real(8),intent(in) :: ener
    ! Temperature in MeV
    real(8),intent(in) :: temp_mev
    ! degeneracy parrameter of electrons
    real(8),intent(in) :: eta_e
    ! chemical potential difference of neutron and proton (mu_n-mu_p) in MeV
    real(8),intent(in) :: muhat
    
    real(8) :: feminus_exp_log10ed,SA_exp_log10ed,feminus_over_SA_exp_log10ed !dimensionless
    real(8) :: one_plus_feminus_exp_log10ed,one_plus_SA_exp_log10ed !dimensionless
    real(8) :: weak_mag !dimensionless
    real(8) :: logterm,expterm !dimensionless
  
    real(8) :: ener_ele, fermi_ele, fac, logfac, mu_nu_eq

    ! function
    real(8) :: fermidirac_exptermonly_log10ed
    
    ! final electron energy
    ener_ele = ener+dmnp
    ! neutrino chemical potential in thermal equilibrium
    mu_nu_eq = eta_e*temp_mev - muhat
    
    !
    feminus_exp_log10ed = fermidirac_exptermonly_log10ed(ener_ele, &
        eta_e*temp_mev, temp_mev) !final state electron blocking
    SA_exp_log10ed = fermidirac_exptermonly_log10ed(ener,mu_nu_eq,temp_mev)

    weak_mag = weak_mag_corr(ener,0) !to first order 1.0d0+1.01d0*neutrino_energy/m_ref, Horowitz 2002

    !Note on the exp's.  The Fermi functions and simulated absorption
    !terms can be huge/small.  The best way to deal with this is to
    !play tricks.  We write them all in terms of the log of the exp, not
    !the fermi functions, and then combine appropiately (taking into
    !account the size of the exp to deal with the +1's appropiately
    
    !Note, we can also combine the feminus_exp/SA_exp term
    !the arguement of feminus_exp is neutrino_energy + delta_np - matter_mue
    !the arguement of SA_exp is neutrino_energy - matter_nue + matter_muhat
    !feplus_exp/SA_exp = exponential with arguement of
    !delta_np-matter_muhat
    feminus_over_SA_exp_log10ed = (dmnp-muhat)/temp_mev*log10exp

    !The full term we are lumping together is:
    !(1-f_{e-})/(1-f_{\nu_e}^{eq}) =
    !fexp_{e-}*(1+fexp_{SA})/(1+fexp_{e-})/fexp_{SA} =
    !10.0d0**(feminus_over_SA_exp_log10ed - one_plus_feminus_exp_log10ed
    !+ one_plus_SA_exp_log10ed)

    !deal with fermi functions +1's
    if (feminus_exp_log10ed.gt.30.0d0) then !exp >> 1
       one_plus_feminus_exp_log10ed = feminus_exp_log10ed 
    else if (feminus_exp_log10ed.lt.-30.0d0) then !exp << 1
       one_plus_feminus_exp_log10ed = 0.0d0
    else 
       one_plus_feminus_exp_log10ed = log10(1.0d0+10.0d0**(feminus_exp_log10ed))
    endif
    
    if (SA_exp_log10ed.gt.30.0d0) then  !exp >> 1
       one_plus_SA_exp_log10ed = SA_exp_log10ed
    else if (SA_exp_log10ed.lt.-30.0d0) then !exp << 1
       one_plus_SA_exp_log10ed = 0.0d0
    else
       one_plus_SA_exp_log10ed = log10(1.0d0+10.0d0**(SA_exp_log10ed))
    endif
    
    logterm = min(200.0d0,max(-200.0d0,feminus_over_SA_exp_log10ed - one_plus_feminus_exp_log10ed + one_plus_SA_exp_log10ed))
    expterm = 10.0d0**(logterm)

    expterm = (1d0 - 1d0/(1.0d0+10.0d0**(feminus_exp_log10ed)))/(1d0-1d0/(1.0d0+10.0d0**(SA_exp_log10ed)))

    ! units of cm^2
    crosssection=sigma0*(1d0+3d0*ga2)/4d0 * (ener_ele/memev)**2 * sqrt(1d0 - (memev/ener_ele)**2) * expterm * weak_mag
    
  end function crosssection_nue_n_abs_incl_stimulated


  real(8) function crosssection_nueb_p_abs_incl_stimulated(ener, temp_mev, eta_e, muhat) result(crosssection)
    use const
    ! energy of neutrino in MeV
    real(8),intent(in) :: ener
    ! Temperature in MeV
    real(8),intent(in) :: temp_mev
    ! degeneracy parrameter of electrons
    real(8),intent(in) :: eta_e
    ! chemical potential difference of neutron and proton (mu_n-mu_p) in MeV
    real(8),intent(in) :: muhat
    
    real(8) :: feplus_exp_log10ed,SA_exp_log10ed,feplus_over_SA_exp_log10ed !dimensionless
    real(8) :: one_plus_feplus_exp_log10ed,one_plus_SA_exp_log10ed !dimensionless
    real(8) :: weak_mag !dimensionless
    real(8) :: logterm,expterm !dimensionless
  
    real(8) :: ener_pos, fermi_ele, fac, logfac, mu_nu_eq

    ! function
    real(8) :: fermidirac_exptermonly_log10ed
    
    ! final positron energy
    ener_pos = ener-dmnp
    if (ener_pos < memev)then
       crosssection = 0d0
       return
    endif
    
    ! antineutrino chemical potential in thermal equilibrium
    mu_nu_eq = -(eta_e*temp_mev - muhat)
    
    
    feplus_exp_log10ed = fermidirac_exptermonly_log10ed(ener_pos, &
        -eta_e*temp_mev, temp_mev) !final state electron blocking
    SA_exp_log10ed = fermidirac_exptermonly_log10ed(ener,mu_nu_eq,temp_mev)
    
    weak_mag = weak_mag_corr(ener,1) !to first order 1.0d0+1.01d0*neutrino_energy/m_ref, Horowitz 2002

    !Note on the exp's.  The Fermi functions and simulated absorption
    !terms can be huge/small.  The best way to deal with this is to
    !play tricks.  We write them all in terms of the log of the exp, not
    !the fermi functions, and then combine appropiately (taking into
    !account the size of the exp to deal with the +1's appropiately
    
    !Note, we can also combine the feplus_exp/SA_exp term
    !the arguement of feplus_exp is neutrino_energy + delta_np - matter_mue
    !the arguement of SA_exp is neutrino_energy - matter_nue + matter_muhat
    !feplus_exp/SA_exp = exponential with arguement of
    !delta_np-matter_muhat
    feplus_over_SA_exp_log10ed = -(dmnp-muhat)/temp_mev*log10exp

    !The full term we are lumping together is:
    !(1-f_{e-})/(1-f_{\nu_e}^{eq}) =
    !fexp_{e-}*(1+fexp_{SA})/(1+fexp_{e-})/fexp_{SA} =
    !10.0d0**(feplus_over_SA_exp_log10ed - one_plus_feplus_exp_log10ed
    !+ one_plus_SA_exp_log10ed)

    !deal with fermi functions +1's
    if (feplus_exp_log10ed.gt.30.0d0) then !exp >> 1
       one_plus_feplus_exp_log10ed = feplus_exp_log10ed 
    else if (feplus_exp_log10ed.lt.-30.0d0) then !exp << 1
       one_plus_feplus_exp_log10ed = 0.0d0
    else 
       one_plus_feplus_exp_log10ed = log10(1.0d0+10.0d0**(feplus_exp_log10ed))
    endif
    
    if (SA_exp_log10ed.gt.30.0d0) then  !exp >> 1
       one_plus_SA_exp_log10ed = SA_exp_log10ed
    else if (SA_exp_log10ed.lt.-30.0d0) then !exp << 1
       one_plus_SA_exp_log10ed = 0.0d0
    else
       one_plus_SA_exp_log10ed = log10(1.0d0+10.0d0**(SA_exp_log10ed))
    endif
    
    logterm = min(200.0d0,max(-200.0d0,feplus_over_SA_exp_log10ed - one_plus_feplus_exp_log10ed + one_plus_SA_exp_log10ed))
    expterm = 10.0d0**(logterm)

    crosssection=sigma0*(1d0+3d0*ga2)/4d0 * (ener_pos/memev)**2 * sqrt(1d0 - (memev/ener_pos)**2) * expterm * weak_mag
    
  end function crosssection_nueb_p_abs_incl_stimulated


  real(8) function weak_mag_corr(ener,type)
    ! neutrino energy in MeV
    real(8),intent(in) :: ener
    ! type=0 for nue+neutron, type=1 for nueb+proton
    integer,intent(in) :: type
    
    real(8) :: c_v,c_a,F2,e,s
    
    c_v = 1.0d0
    c_a = ga
    F2 = 3.706d0
    
    if (type == 0) then !neutrino capture crosssection on neutron
       e = ener/mnmev
       s = +1d0
       ! weak_mag_corr_abs = (c_v**2*(1.0d0+4.0d0*e+16.0d0/3.0d0*e**2)+ &
       !      3.0d0*c_a**2*(1.0d0+4.0d0/3.0d0*e)**2+4.0d0*(c_v+F2)*c_a*e* &
       !      (1.0d0+4.0d0/3.0d0*e)+8.0d0/3.0d0*c_v*F2*e**2+&
       !      5.0d0/3.0d0*e**2*(1.0d0+2.0d0/5.0d0*e)*F2**2)/ &
       !      ((c_v**2+3.0d0*c_a**2)*(1.0d0+2.0d0*e)**3)
    else if (type == 1) then !antineutrino capture crosssection on proton
       e = ener/mpmev
       s = -1d0
       ! weak_mag_corr_abs = (c_v**2*(1.0d0+4.0d0*e+16.0d0/3.0d0*e**2)+ &
       !      3.0d0*c_a**2*(1.0d0+4.0d0/3.0d0*e)**2-4.0d0*(c_v+F2)*c_a*e* &
       !      (1.0d0+4.0d0/3.0d0*e)+8.0d0/3.0d0*c_v*F2*e**2+&
       !      5.0d0/3.0d0*e**2*(1.0d0+2.0d0/5.0d0*e)*F2**2)/ &
       !      ((c_v**2+3.0d0*c_a**2)*(1.0d0+2.0d0*e)**3)
    else
       stop "weak_mag_correction_absorption: Wrong type"
    endif

    weak_mag_corr = (c_v**2*(1.0d0+4.0d0*e+16.0d0/3.0d0*e**2)+ &
         3.0d0*c_a**2*(1.0d0+4.0d0/3.0d0*e)**2+s*4.0d0*(c_v+F2)*c_a*e* &
         (1.0d0+4.0d0/3.0d0*e)+8.0d0/3.0d0*c_v*F2*e**2+&
         5.0d0/3.0d0*e**2*(1.0d0+2.0d0/5.0d0*e)*F2**2)/ &
         ((c_v**2+3.0d0*c_a**2)*(1.0d0+2.0d0*e)**3)
       
  end function weak_mag_corr

  subroutine nnbrems_spectra(temp,ener,spectr_nu)
    use const
    ! temperature in MeV
    real(8),intent(in) :: temp
    ! neutrino energy in MeV
    real(8),intent(in) :: ener
    ! normalized spectrum in units of 1/MeV/srad.
    ! Multiply Q_brem (erg/s/cm^3)
    real(8),intent(out) :: spectr_nu
    
    real(8) :: x
    
    x=ener/temp
    spectr_nu = 0.2319066437d0/(4d0*pi*temp) * x**2.4d0 * exp(-1.1d0*x)

  end subroutine nnbrems_spectra


end module module_weak_spectral
