# Makefile

EXE_DIR := bin/
# program file name

# TARGET1_SERIAL := ${EXE_DIR}serial.out
TARGET1 := ${EXE_DIR}a.out

# compiler

# for MPI run, select mpi-supporting compiler and add MOD_MPI and SRC_MPI to SRC
#FXX := ifort
#FXX := gfortran
# FXX := mpiifort
# FXX := h5pfc
FXX := h5fc
# FXX_2 := mpiifort
# FXX_2 := h5pfc

# compiler option
# # ifort option
#FFLAGS:=-convert big_endian -mcmodel=large -shared-intel -fpic -qopenmp -xCORE-AVX512 -qopt-zmm-usage=high -xHost -qopt-report 5 
#FFLAGS0+=-O0 -CB -traceback -g -fpe0 -check uninit #-warn unused
#FFLAGS+= -diag-disable=10121 #-z noexecstack #-h ipafrom=vis_fcn.f90:atm_fnc.f90
#FFLAGS+=-diag-disable:5462
# gfortran flags
FFLAGS:=-ffree-line-length-none -cpp #-pg  #-fbacktrace -O0 -g -fcheck=all -ffpe-trap=invalid,zero,overflow,underflow,denormal -ffpe-summary=all
#FFLAGS+=-fopenmp
# FFLAGS+=-fbacktrace -O0 -g -fcheck=all -ffpe-trap=invalid,zero,overflow,underflow,denormal -ffpe-summary=all
# FFLAGS+= -fbacktrace -O0 -g -fbounds-check -fcheck=all -ffpe-trap=invalid,zero #,overflow,underflow
#FFLAGS+=-fbounds-check -O -Wuninitialized -Wall # -fbacktrace -g  -ffpe-trap=invalid,zero,overflow,underflow,denormal
#FFLAGS:=-fconvert=big-endian -ffree-line-length-none -cpp -fopenmp #-pg  #-fbacktrace -O0 -g -fcheck=all -ffpe-trap=invalid,zero,overflow,underflow,denormal -ffpe-summary=all


# library link
#LIBS := -mkl

# suffix rule
.SUFFIXES: .f90 .F90 .o .mod
%.o: %.mod

# source file

# MOD := const_mod.f90 phys_const_mod.f90 module_fermi.f90 module_weak_interaction.f90 module_tabeos.f90 module_pair_rate.f90 module_itoh.f90
MOD := const_mod.f90 phys_const_mod.f90 module_fermi.f90 module_weak_energy_integrated.f90 module_weak_spectral.f90 module_itoh.f90
SRC := test_rate.f90 illinois_rootfinding.f90 nulib_fermi.f90
SRC_TIMMES = timeos_electron.f90 timeos.f90 #sneut5.f90 enumber.f90

SRC_DIR := src/
TIM_DIR := timmes_routines/

OBJ_DIR := obj/

#FFLAGS += -module $(OBJ_DIR)
FFLAGS += -J$(OBJ_DIR)

SRC := $(MOD) $(SRC)

SRC := $(addprefix $(SRC_DIR), $(SRC)) $(addprefix $(TIM_DIR), $(SRC_TIMMES))

OBJ_FILES := $(addprefix $(OBJ_DIR),$(notdir $(SRC:.f90=.o)))
MOD_FILES := $(addprefix $(OBJ_DIR),$(notdir $(MOD:.f90=.mod)))


.PHONY: all dirs test


all : dirs $(TARGET1) $(TARGET2)
# all : dirs $(TARGET2)

objs : dirs $(OBJ_FILES)

dirs : $(EXE_DIR) $(OBJ_DIR)

$(EXE_DIR):
	mkdir -p $(EXE_DIR)

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

$(TARGET1): $(OBJ_FILES)
	$(FXX) $(FFLAGS) -o $@ $(OBJ_FILES) $(OMPFLAGS) $(LIBS)

$(OBJ_DIR)%.o: $(SRC_DIR)%.F90
	$(FXX) $(FFLAGS) $(OMPFLAGS) $(LIBS) -c $< -o $@

$(OBJ_DIR)%.o: $(TIM_DIR)%.F90
	$(FXX) $(FFLAGS) $(OMPFLAGS) $(LIBS) -c $< -o $@

$(OBJ_DIR)%.mod: $(SRC_DIR)%.F90
	$(FXX) $(FFLAGS) $(OMPFLAGS) $(LIBS) -c $< -o $@

$(OBJ_DIR)%.mod: $(TIM_DIR)%.F90
	$(FXX) $(FFLAGS) $(OMPFLAGS) $(LIBS) -c $< -o $@


test:
	@echo $(SRC)
	@echo $(OBJ_FILES)


# clean rule
.PHONY: clean
clean:
	$(RM) $(OBJ_DIR)*.o $(OBJ_DIR)*.mod fort*

